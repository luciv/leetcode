class ListNode {
    constructor(val, next) {
        this.val = (val === undefined ? 0 : val);
        this.next = (next === undefined ? null : next);
    }
}
 
var addTwoNumbers = function(l1, l2) {
    let head = new ListNode(0);
    let p = l1; 
    let q = l2; 
    let cur = head;
    let s = 0;
    while (p != null || q != null) {
        let x = (p != null) ? p.val : 0;
        let y = (q != null) ? q.val : 0;
        let sum = s + x + y;
        s = Math.floor(sum / 10);
        cur.next = new ListNode(sum % 10);
        cur = cur.next;
        if (p != null) p = p.next;
        if (q != null) q = q.next;
    }
    if (s > 0) {
        cur.next = new ListNode(s);
    }
    return head.next;
};