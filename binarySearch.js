const nums = [2, 5]
const target = 2;

var search = function(nums, target) {
    let pivot = 0; 
    let left = 0;
    let right = nums.length - 1;
    while (left <= right) {
        pivot = Math.floor((right - left) / 2);
        if (nums[pivot] == target) {
            return pivot
        }
        if (nums[pivot] > target) {
            right = pivot - 1;
        } else {
            left = pivot + 1;
        }
    }
    return -1;
}

console.log(search(nums, target));

