function mergeSortedArray(a,b){
    var tempArray = [];
    while(a.length || b.length) {
        if(typeof a[0] === 'undefined') {
            tempArray.push(b[0]);
            b.splice(0,1);
        } else if(a[0] > b[0]){
            tempArray.push(b[0]);
            b.splice(0,1);
        } else {
            tempArray.push(a[0]);
            a.splice(0,1);
        }
    }
    return tempArray;
}

var findMedianSortedArrays = function(nums1, nums2) {
    const nums = mergeSortedArray(nums1, nums2);
    let l = Math.trunc((nums.length - 1) / 2);
    return (nums.length % 2) ? nums[l] : (nums[l] + nums[l + 1]) / 2;
};
console.log(findMedianSortedArrays([4], []));