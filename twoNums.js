let twoSum = (nums, target) => {
    numsMap = new Map();
    for (i = 0; i < nums.length; i++) {
        const dif = target - nums[i];
        const index = numsMap.get(nums[i]);
        if (index != undefined) {
              return [i, index];
        }
        numsMap.set(dif, i);
    }
}



console.log(twoSum([2, 7, 11, 15], 9));